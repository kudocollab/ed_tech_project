<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student', function () {
    return view('student');
})->middleware('check_student');

Route::get('/teacher', function () {
    return view('teacher');
})->middleware('check_teacher');

Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/register', 'Auth\LoginController@register');

Route::get('/students/{course_id}', 'UsersController@getStudents');

Route::get('/quizzes/{course_id}', 'QuizzesController@getQuizzes');
Route::put('/quizzes/{course_id}', 'QuizzesController@createQuiz');

Route::get('/student_quizzes/{course_id}/{user_id}', 'QuizzesController@getStudentQuizzes');

Route::get('/quiz/{quiz_id}', 'QuizzesController@getQuiz');
Route::post('/quiz/{quiz_id}/{user_id}', 'QuizzesController@submitQuiz');

Route::post('/quizresponse/{quiz_id}/{user_id}/{question_id}', 'QuizzesController@submitResponse');
