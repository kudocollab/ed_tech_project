<?php

use Illuminate\Database\Seeder;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use App\Enums\UserType;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Student::truncate();
    	Teacher::truncate();
    	User::truncate();

    	$teacher_1_user = new User();
    	$teacher_1_user->username = 'rachael.minter';
    	$teacher_1_user->password = bcrypt('password');
    	$teacher_1_user->first_name = 'Rachael';
    	$teacher_1_user->last_name = 'Minter';
    	$teacher_1_user->user_type = UserType::$teacher;
    	$teacher_1_user->save();

    	$teacher_1 = new Teacher();
    	$teacher_1->user_id = $teacher_1_user->id;
    	$teacher_1->save();

    	$student_1_user = new User();
    	$student_1_user->username = 'brian.minter';
    	$student_1_user->password = bcrypt('password');
    	$student_1_user->first_name = 'Brian';
    	$student_1_user->last_name = 'Minter';
    	$student_1_user->user_type = UserType::$student;
    	$student_1_user->save();

    	$student_1 = new Student();
    	$student_1->user_id = $student_1_user->id;
    	$student_1->teacher_id = $teacher_1->id;
    	$student_1->save();

    	$student_2_user = new User();
    	$student_2_user->username = 'steven.minter';
    	$student_2_user->password = bcrypt('password');
    	$student_2_user->first_name = 'Steven';
    	$student_2_user->last_name = 'Minter';
    	$student_2_user->user_type = UserType::$student;
    	$student_2_user->save();

    	$student_2 = new Student();
    	$student_2->user_id = $student_2_user->id;
    	$student_2->teacher_id = $teacher_1->id;
    	$student_2->save();
    }
}
