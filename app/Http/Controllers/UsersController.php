<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Enums\UserType;

class UsersController extends Controller
{
    public function getStudents($course_id)
    {
    	return User::where([['course_id', $course_id], ['user_type', UserType::$student]])->
    	orderBy('last_name', 'asc')->
    	orderBy('first_name', 'asc')->
    	orderBy('username', 'asc')->
    	get();
    }
}
