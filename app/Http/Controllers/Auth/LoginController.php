<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Route;
use Session;
use App\Models\User;
use App\Models\Course;
use App\Models\Student;
use App\Models\Teacher;
use App\Enums\UserType;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username'   => 'required',
            'password' => 'required',
        ]);

        if(Auth::guard('user')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {
            $user = Auth::user();

            if($user->user_type == UserType::$student) {
                return 'REDIRECT_STUDENT';
            } else if($user->user_type == UserType::$teacher) {
                return 'REDIRECT_TEACHER';
            }
        }

        return 'INCORRECT_CREDENTIALS';
    }

    public function logout()
    {
        Auth::guard('user')->logout();
        return redirect('/');
    }

    public function register()
    {
        $username = request('username');
        $password = request('password');
        $user_type = request('user_type');
        $course_code = request('course_code');

        if(strlen($password) < 7) {
            return "PASSWORD_TOO_SHORT";
        }else if($user_type == UserType::$student) {
            $course = Course::where('code', $course_code)->first();

            if(empty($course)) {
                return "CLASS_NOT_FOUND";
            }

            $user = User::where([['username', $username], ['course_id', $course->id]])->first();

            if(!empty($user)) {
                return 'USERNAME_TAKEN';
            }

            $new_user = new User();
            $new_user->username = $username;
            $new_user->password = bcrypt($password);
            $new_user->course_id = $course->id;
            $new_user->user_type = UserType::$student;
            $new_user->save();

            Auth::guard('user')->attempt(['username' => $username, 'password' => $password]);

            return "REDIRECT_STUDENT";
        } else if ($user_type == UserType::$teacher) {
            $new_course = new Course();

            $new_code = '';
            $count = 1;
            do {
                $new_code = strtoupper(substr(uniqid(), 0, 6));
            } while(!empty(Course::where('code', $new_code)->first()) && $count++ < 5);


            $new_course->code = $new_code;
            $new_course->save();

            $new_user = new User();
            $new_user->username = $username;
            $new_user->password = bcrypt($password);
            $new_user->course_id = $new_course->id;
            $new_user->user_type = UserType::$teacher;
            $new_user->save();

            Auth::guard('user')->attempt(['username' => $username, 'password' => $password]);

            return "REDIRECT_TEACHER";
        }
    }
}
