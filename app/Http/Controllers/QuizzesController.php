<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quiz;
use App\Models\QuizQuestion;
use App\Models\QuizResponse;
use App\Models\QuizResult;
use App\Enums\QuizType;
use Illuminate\Support\Facades\DB;
use App\Events\ResponseSent;
use App\Traits\AdditionITS;
use App\Traits\SubtractionITS;

class QuizzesController extends Controller
{
	use AdditionITS, SubtractionITS;

	public function getQuizzes($course_id)
	{
		return Quiz::where('course_id', $course_id)->
		with(array('quiz_questions' => function($query) {
			$query->with('quiz_responses');
		}))->
		with('quiz_results')->
		orderBy('created_at')->
		get();
	}

	public function getStudentQuizzes($course_id, $user_id)
	{
		return Quiz::where('course_id', $course_id)->
		with(array('quiz_questions' => function($query) use($user_id) {
			$query->with(array('quiz_responses' => function($query_2) use ($user_id) {
				$query_2->where('user_id', $user_id)->get();
			}));
		}))->
		with(array('quiz_results' => function($query) use($user_id) {
			$query->where('user_id', $user_id)->get();
		}))->
		orderBy('created_at')->
		get();
	}

	public function getQuiz($quiz_id)
	{
		return Quiz::where('id', $quiz_id)->
		with(array('quiz_questions' => function($query) {
			$query->with('quiz_responses');
		}))->
		first();
	}

	public function createQuiz($course_id)
	{
		$quiz_type = request('quiz_type');

		if($quiz_type == QuizType::$addition) {
			$quiz = new Quiz();
			$quiz->course_id = $course_id;
			$quiz->quiz_type = $quiz_type;
			$quiz->is_retakable = true;
			$most_recent_quiz = Quiz::where('quiz_type', QuizType::$addition)->latest()->first();
			$quiz->number = empty($most_recent_quiz) ? 1 : $most_recent_quiz->number + 1;
			$quiz->save();

			for ($i=0; $i < 12; $i++) {
				$number_1 = rand(10, 100);
				$number_2 = rand(10, 100);

				$quiz_question = new QuizQuestion;
				$quiz_question->quiz_id = $quiz->id;
				$quiz_question->question = $number_1.' + '.$number_2.' =';
				$quiz_question->answer = $number_1 + $number_2;
				$quiz_question->save();
			}

		} else if($quiz_type == QuizType::$subtraction) {
			$quiz = new Quiz();
			$quiz->course_id = $course_id;
			$quiz->quiz_type = $quiz_type;
			$quiz->is_retakable = true;
			$most_recent_quiz = Quiz::where('quiz_type', QuizType::$subtraction)->latest()->first();
			$quiz->number = empty($most_recent_quiz) ? 1 : $most_recent_quiz->number + 1;
			$quiz->save();

			for ($i=0; $i < 12; $i++) {
				$number_1 = rand(10, 100);
				$number_2 = rand(10, 100);

				if($number_1 < $number_2) {
					$temp = $number_1;
					$number_1 = $number_2;
					$number_2 = $temp;
				}

				$quiz_question = new QuizQuestion;
				$quiz_question->quiz_id = $quiz->id;
				$quiz_question->question = $number_1.' - '.$number_2.' =';
				$quiz_question->answer = $number_1 - $number_2;
				$quiz_question->save();
			}
		}
	}

	public function submitQuiz($quiz_id, $user_id)
	{
		$quiz_result = new QuizResult();
		$quiz_result->quiz_id = $quiz_id;
		$quiz_result->user_id = $user_id;
		$quiz_result->correct = QuizResponse::whereIn('quiz_question_id', QuizQuestion::where('quiz_id', $quiz_id)->pluck('id')->toArray())->
		where([['user_id', $user_id], ['correct', true]])->
		count();
		$quiz_result->total_questions = QuizQuestion::where('quiz_id', $quiz_id)->count();
		$quiz_result->save();
	}

	function comp($first, $second) {
		return len($first) > len($second);
	}

	public function submitResponse($quiz_id, $user_id, $question_id)
	{
		QuizResponse::where([['user_id', $user_id], ['quiz_question_id', $question_id]])->delete();

		$response_answer = trim(request('response'));
		$response = new QuizResponse();
		$response->quiz_question_id = $question_id;
		$response->user_id = $user_id;
		$response->correct = $response_answer == (QuizQuestion::where('id', $question_id)->first())->answer;
		$response->response = $response_answer;
		
		$quiz = Quiz::where('id', $quiz_id)->first();

		if(!$response->correct) {
			$quiz_question = QuizQuestion::where('id', $question_id)->first();

			$exploded_array = explode(' ', $quiz_question->question);
			$first_number = $exploded_array[0];
			$second_number = $exploded_array[2];

			$result;
			if($quiz->quiz_type == QuizType::$addition) {
				$result = AdditionITS::test_addition($first_number, $second_number, $response_answer);
			} else {
                $result = SubtractionITS::test_subtraction($first_number, $second_number, $response_answer);
            }
			usort($result, function($first, $second) {
				return count($first) > count($second);
			});

			$top_reason = $result[0];

			$second_reason = null;
			for($i = 1; $i < count($result); $i++) {
				if($result[$i] !== $top_reason) {
					$second_reason = $result[$i];
				}
			}
			$top_reasons = array($top_reason, $second_reason);
			// echo json_encode($top_reasons);
			$response->reasons = json_encode($top_reasons);
		}

		$response->save();

		$quiz = Quiz::where('id', $quiz_id)->
		with(array('quiz_questions' => function($query) use($user_id) {
			$query->with('quiz_responses');
		}))->
		with('quiz_results')->
		first();

		broadcast(new ResponseSent($quiz));

		return $quiz;
	}
}
