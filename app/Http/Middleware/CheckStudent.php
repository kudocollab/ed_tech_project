<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Enums\UserType;

class CheckStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if(Auth::user()->user_type == UserType::$student) {
                return $next($request);
            }
        }
        return redirect('/');
    }
}
