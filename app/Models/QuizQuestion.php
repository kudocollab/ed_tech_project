<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model
{
    public function quiz_responses()
    {
    	return $this->hasMany('App\Models\QuizResponse', 'quiz_question_id');
    }
}
