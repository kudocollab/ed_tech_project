<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function quiz_questions()
    {
        return $this->hasMany('App\Models\QuizQuestion', 'quiz_id');
    }

    public function quiz_results()
    {
        return $this->hasMany('App\Models\QuizResult', 'quiz_id');
    }
}
