<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $hidden = ['password', 'remember_token'];

    public function course()
    {
    	return $this->hasOne('App\Models\Course', 'id', 'course_id');
    }
}
