<?php

namespace App\Enums;

class UserType {
	public static $student = 1;
	public static $teacher = 2;
}

