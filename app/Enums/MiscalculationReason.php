<?php

namespace App\Enums;

interface MiscalculationReason
{
    const none = 0;
    const miscalculated_column_1 = 1;
    const miscalculated_column_2 = 2;
    const miscalculated_column_3 = 3;
    const miscalculated_column_4 = 4;
    const miscalculated_column_5 = 5;
    const did_not_answer = 6;
    const did_not_add_all_columns = 7;
    const forgot_to_carry_the_one_from_column_1 = 8;
    const forgot_to_carry_the_one_from_column_2 = 9;
    const forgot_to_carry_the_one_from_column_3 = 10;
    const forgot_to_carry_the_one_from_column_4 = 11;
    const forgot_to_carry_the_one_from_column_5 = 12;
    const forgot_to_add_the_one = 13;
    const carried_the_one_needlessly = 14;
    const unknown = 15;
    const forgot_to_borrow_the_one = 16;
    const forgot_to_borrow_the_one_from_column_2 = 17;
    const forgot_to_borrow_the_one_from_column_3 = 18;
    const forgot_to_borrow_the_one_from_column_4 = 19;
    const forgot_to_borrow_the_one_from_column_5 = 20;
    const forgot_to_borrow_the_one_from_column_6 = 21;
    const borrowed_the_one_needlessly = 22;
}
