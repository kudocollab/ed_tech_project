<?php

namespace App\Traits;

use App\Enums\MiscalculationReason;

trait AdditionITS {

	public static function test_addition($first_number, $second_number, $student_answer) {
		global $answer_map;

		if(trim($student_answer) == '') {
			return array(array(MiscalculationReason::did_not_answer));
		}

		$answer_map = array();

		$answer_map[''] = array(MiscalculationReason::did_not_answer);

		$first_number_string = strval($first_number);
		$second_number_string = strval($second_number);
		$student_answer_string = strval($student_answer);

		$current_student_miscalculations = array();

		self::add_column($current_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, '', 0, 0);

		usort($answer_map[$student_answer], function($first, $second) {
			return count($first) > count($second);
		});

		return $answer_map[$student_answer];
	}

	public static function add_column($current_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $current_student_answer, $column_number, $carry_over) {
		$first_number_value = (int)$first_number_string;
		$second_number_value = (int)$second_number_string;

		$first_number_column_number = 0;
		$second_number_column_number = 0;

		if($column_number + 1 <= strlen($first_number_string))
			$first_number_column_number = substr($first_number_string, -1 - $column_number, 1);

		if($column_number + 1 <= strlen($second_number_string))
			$second_number_column_number = substr($second_number_string, -1 - $column_number, 1);

		if(($first_number_column_number == 0 && $second_number_column_number == 0 && $carry_over == 0) || max(strlen($first_number_string), strlen($second_number_string)) < $column_number) {
			self::insert_miscalculation_reason($current_student_answer, $current_student_miscalculations);
			return;
		}

		$correct_column_value = substr(strval($first_number_value + $second_number_value), -1 - $column_number, 1);
		for ($i = max($correct_column_value - 2, 0); $i < $correct_column_value + 3; $i++) {
			$temp_crruent_student_answer = ($i % 10).$current_student_answer;
			$temp_student_miscalculations = $current_student_miscalculations;

			if(($i % 10) != $correct_column_value) {
				if(($i + 1 % 10) == $correct_column_value && $carry_over == 1) {
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_add_the_one);
				} else if(end($temp_student_miscalculations) >= MiscalculationReason::forgot_to_carry_the_one_from_column_1 && end($temp_student_miscalculations) <= MiscalculationReason::forgot_to_carry_the_one_from_column_5 && $carry_over == 0 && ($i - 1 % 10)) {

				} else {
					switch ($column_number) {
						case 0:
						array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_1);
						break;
						case 1:
						array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_2);
						break;
						case 2:
						array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_3);
						break;
						case 3:
						array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_4);
						break;
						case 4:
						array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_5);
						break;
					}
				}
			}

			if((int)$first_number_column_number + (int) $second_number_column_number >= 10) {
				self::add_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 1);

				switch ($column_number) {
					case 0:
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_carry_the_one_from_column_1);
					break;
					case 1:
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_carry_the_one_from_column_2);
					break;
					case 2:
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_carry_the_one_from_column_3);
					break;
					case 3:
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_carry_the_one_from_column_4);
					break;
					case 4:
					array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_carry_the_one_from_column_5);
					break;
				}

				self::add_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 0);

			} else {
				self::add_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 0);

				// array_push($temp_student_miscalculations, MiscalculationReason::carried_the_one_needlessly);

				// self::add_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 1);
			}

			self::add_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 0);
		}
	}

	public static function insert_miscalculation_reason($current_student_answer, $miscalculation_reasons) {
		global $answer_map;

		if(array_key_exists($current_student_answer, $answer_map)) {
			$duplicate = false;
			foreach ($answer_map[ltrim($current_student_answer, '0')] as $reason) {
				if(count(array_diff($reason, $miscalculation_reasons)) == 0) {
					$duplicate = true;
				}
			}
			if(!$duplicate)
				array_push($answer_map[ltrim($current_student_answer, '0')], $miscalculation_reasons);
		} else {
			$answer_map[ltrim($current_student_answer, '0')] = array($miscalculation_reasons);
		}
	}
}
