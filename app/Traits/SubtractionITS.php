<?php

namespace App\Traits;

use App\Enums\MiscalculationReason;

trait SubtractionITS
{
    public static function test_subtraction($first_number, $second_number, $student_answer)
    {
        global $answer_map;

        if (trim($student_answer) == '') {
            return array(array(MiscalculationReason::did_not_answer));
        }

        $answer_map = array();

        $answer_map[''] = array(MiscalculationReason::did_not_answer);

        $first_number_string = strval($first_number);
        $second_number_string = strval($second_number);
        $student_answer_string = strval($student_answer);

        $current_student_miscalculations = array();

        self::subtract_column($current_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, '', 0, 0);

        usort($answer_map[$student_answer], function ($first, $second) {
            return count($first) > count($second);
        });
        return $answer_map[$student_answer];
    }

    public static function subtract_column($current_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $current_student_answer, $column_number, $borrow)
    {
        $first_number_value = (int)$first_number_string;
        $second_number_value = (int)$second_number_string;

        $first_number_column_number = 0;
        $second_number_column_number = 0;

        if ($column_number + 1 <= strlen($first_number_string)) {
            $first_number_column_number = substr($first_number_string, -1 - $column_number, 1);
        }

        if ($column_number + 1 <= strlen($second_number_string)) {
            $second_number_column_number = substr($second_number_string, -1 - $column_number, 1);
        }

        if (($first_number_column_number == 0 && $second_number_column_number == 0 && $borrow == 0) || max(strlen($first_number_string), strlen($second_number_string)) < $column_number + 1) {
            self::insert_miscalculation_subtraction_reason($current_student_answer, $current_student_miscalculations);
            return;
        }
        if ($column_number + 1 > strlen(strval($first_number_value - $second_number_value))) {
            $correct_column_value = 0;
        } else {
            $correct_column_value = substr(strval($first_number_value - $second_number_value), -1 - $column_number, 1);
        }
        // echo $correct_column_value;
        for ($i = max($correct_column_value - 2, 0); $i < $correct_column_value + 2; $i++) {
            if ($column_number == 2) {
                echo $correct_column_value." ".$i."\n";
            }
            $temp_crruent_student_answer = ($i % 10).$current_student_answer;
            $temp_student_miscalculations = $current_student_miscalculations;

            if ((($i + 10) % 10) != $correct_column_value) {
                if (($i + 9 % 10) == $correct_column_value && $borrow == 1) {
                    array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one);
                } elseif (end($temp_student_miscalculations) >= MiscalculationReason::forgot_to_borrow_the_one_from_column_2 && end($temp_student_miscalculations) <= MiscalculationReason::forgot_to_borrow_the_one_from_column_6 && $borrow == 0) {
                } else {
                    switch ($column_number) {
                    case 0:
                    array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_1);
                    break;
                    case 1:
                    array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_2);
                    break;
                    case 2:
                    array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_3);
                    break;
                    case 3:
                    array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_4);
                    break;
                    case 4:
                    array_push($temp_student_miscalculations, MiscalculationReason::miscalculated_column_5);
                    break;
                }
                }
            }

            if ((int)$first_number_column_number - (int) $second_number_column_number < 0) {
                self::subtract_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 1);

                switch ($column_number) {
                case 0:
                array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one_from_column_2);
                break;
                case 1:
                array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one_from_column_3);
                break;
                case 2:
                array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one_from_column_4);
                break;
                case 3:
                array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one_from_column_5);
                break;
                case 4:
                array_push($temp_student_miscalculations, MiscalculationReason::forgot_to_borrow_the_one_from_column_6);
                break;
            }

                self::subtract_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 0);
            } else {
                self::subtract_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 0);

                // array_push($temp_student_miscalculations, MiscalculationReason::borrowed_the_one_needlessly);

                self::subtract_column($temp_student_miscalculations, $first_number_string, $second_number_string, $student_answer_string, $temp_crruent_student_answer, $column_number + 1, 1);
            }
        }
    }

    public static function insert_miscalculation_subtraction_reason($current_student_answer, $miscalculation_reasons)
    {
        global $answer_map;

        if (array_key_exists($current_student_answer, $answer_map)) {
            array_push($answer_map[ltrim($current_student_answer, '0')], $miscalculation_reasons);
        } else {
            $answer_map[ltrim($current_student_answer, '0')] = array($miscalculation_reasons);
        }
    }
}

