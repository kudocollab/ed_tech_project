@extends('master', ['title' => 'Student'])

@section('master-content')

<?php
$student = App\Models\User::where('id', Auth::user()->id)->with('course')->first();
?>

<div id="app">
	<nav-bar course-code="{{ $student->course->code }}"></nav-bar>
	<div class="row">
		<div class="col s8 offset-s2">
			<student-quiz-list course-id="{{ $student->course_id }}" user-id="{{ $student->id }}"></student-quiz-list>
		</div>
	</div>

	<quiz-modal user-id="{{ $student->id }}"></quiz-modal>
</div>
@endsection

@section('extra-scripts')
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript">
	$('.dismissible-modal').modal();
</script>
@endsection
