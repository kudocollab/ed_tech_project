@extends('master', ['title' => 'Project'])

@section('master-content')

<div id="app">
    <login></login>
</div>
@endsection

@section('extra-scripts')
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript">
	$('.dismissible-modal').modal();
	$('ul.tabs').tabs();
</script>
@endsection
