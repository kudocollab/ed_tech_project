@extends('master', ['title' => 'Teacher'])

@section('master-content')

<?php
$teacher = App\Models\User::where('id', Auth::user()->id)->with('course')->first();
?>

<div id="app">
	<nav-bar course-code="{{ $teacher->course->code }}"></nav-bar>

	<div class="row">
		<div class="col s2">

		</div>
		<div class="col s8">
			<ul class="tabs">
				<li class="tab col s6"><a class="active" href="#students">Students</a></li>
				<li class="tab col s6"><a href="#quizzes">Quizzes</a></li>
			</ul>
			<div id="students">
				<student-list :course-id="{{ $teacher->course_id }}"></student-list>
			</div>
			<div id="quizzes">
				<quiz-list :course-id="{{ $teacher->course_id }}"></quiz-list>
			</div>
		</div>
		<div class="col s2">

		</div>
	</div>

	<new-quiz-modal :course-id="{{ $teacher->course_id }}"></new-quiz-modal>

</div>
@endsection

@section('extra-scripts')
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript">
	$('.dismissible-modal').modal();
</script>
@endsection
