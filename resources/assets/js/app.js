window.Laravel = new class {
	constructor() {
		this.csrfToken = document.head.querySelector("[name=csrf-token]").content
	}
}

window.Event = new class {
	constructor() {
		this.vue = new Vue();
	}

	fire(event, data=null) {
		this.vue.$emit(event, data);
	}

	listen(event, callback) {
		this.vue.$on(event, callback);
	}
}

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

import Echo from "laravel-echo"

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '80865543491c2d3e1189',
    cluster: 'us2'
});

Vue.component('login', require('./components/Login.vue'));
Vue.component('loadable-button', require('./components/LoadableButton.vue'));
Vue.component('nav-bar', require('./components/NavBar.vue'));
Vue.component('large-modal', require('./components/LargeModal.vue'));
Vue.component('flat-button', require('./components/FlatButton.vue'));
Vue.component('student-list', require('./components/StudentList.vue'));
Vue.component('loading', require('./components/Loading.vue'));
Vue.component('student', require('./components/Student.vue'));
Vue.component('list-item', require('./components/ListItem.vue'));
Vue.component('quiz-list', require('./components/QuizList.vue'));
Vue.component('quiz', require('./components/Quiz.vue'));
Vue.component('green-click-icon', require('./components/GreenClickIcon.vue'));
Vue.component('dropdown-select', require('./components/DropdownSelect.vue'));
Vue.component('new-quiz-modal', require('./components/NewQuizModal.vue'));
Vue.component('student-quiz', require('./components/StudentQuiz.vue'));
Vue.component('student-quiz-list', require('./components/StudentQuizList.vue'));
Vue.component('primary-button', require('./components/PrimaryButton.vue'));
Vue.component('quiz-modal', require('./components/QuizModal.vue'));
Vue.component('green-icon', require('./components/GreenIcon.vue'));
Vue.component('grey-icon', require('./components/GreyIcon.vue'));
Vue.component('red-icon', require('./components/RedIcon.vue'));

const app = new Vue({
    el: '#app'
});
